import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

public class arrayToFile {

    public static void main(String[] args) throws IOException {

        try {
            File file = new File("D:\\test\\file.txt");

            Scanner scan = new Scanner(file);
            while (scan.hasNext()) {

                String data = scan.nextLine();
                String[] array = data.split("\\s+");

                    for (int i = 0; i < array.length; i++) {
                        for (int j = i + 1; j < array.length; j++) {
                            if (array[j].compareTo(array[i]) < 0) {
                                String k = array[j];
                                array[j] = array[i];
                                array[i] = k;
                            }
                        }
                        System.out.println(array[i]);
                        scan.close();
                    }
                }
            }catch(Exception x){
                x.fillInStackTrace();
        }
    }
}

