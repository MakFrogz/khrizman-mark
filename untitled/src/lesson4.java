public class lesson4 {

    public  static void main(String [] args){



        //task1
        double max = 0;
        double result = 0;

        for (double i = 0; i <= 10 ; i += 0.01){
            result = Math.pow(i, 2);
            if (result > max){
                max = result;
            }
        }
        System.out.println("Task1");
        System.out.println(max);

        //task2

        max = -15;
        for (double j = -15; j < 15; j +=0.01 ){
            result = Math.pow(j, 2) - j +3;
            if (result >max){
                max = result;
            }
        }
        System.out.println("Task2");
        System.out.println(max);


        //task3
        max = -10;
        for (double k = -10; k < 10; k +=0.01){
            result = Math.sin(k) + Math.pow(k, 2);
            if (result >max){
                max = result;
            }
        }
        System.out.println("Task3");
        System.out.println(max);
        //task4
        max = -10;
        for (double l = -10; l < 10; l +=0.01){
            result = Math.cos(l) + Math.pow(l, 2);
            if (result >max){
                max = result;
            }
        }
        System.out.println("Task4");
        System.out.println(max);
        //task5
        max = -50;
        for (double h = -50; h < 50; h +=0.01){
            result = Math.pow(h, 3);
            if (result >max){
                max = result;
            }
        }
        System.out.println("Task5");
        System.out.println(max);

        //task6
        max = 2;
        for (double k = -10; k < 10; k +=0.01) {
            result = 1 / (Math.pow(k, 3));
            if (!(k >= -1 && k <= 1)) {
               // result = 1 / (Math.pow(k, 3));
                if (result > max) {
                    max = result;
                }
            }
        }
        System.out.println("Task6");
        System.out.println(max);

        //task7
        max = -10;
        for (double k = -10; k < 10; k +=0.01) {
            result = (1 / (Math.pow(k, 3))) + Math.pow(k, 3);
            if (!(k >= -1 && k <= 1)) {
                if (result > max) {
                    max = result;
                }
            }
        }
        System.out.println("Task7");
        System.out.println(max);


        //task8
        max = -10;
        for (double k = -10; k < 10; k +=0.01){
            result = Math.exp(k);
            if (result >max){
                max = result;
            }
        }
        System.out.println("Task8");
        System.out.println(max);
        //task9
        max = -100;
        for (double k = -100; k < 100; k +=0.01){
            result = Math.exp(Math.sin(k));
            if (result >max){
                max = result;
            }
        }
        System.out.println("Task9");
        System.out.println(max);

        //task10
        max = -10;
        for (double k = -10; k < 10; k +=0.01){
            result = Math.pow(k, 2.71);
            if (result >max){
                max = result;
            }
        }
        System.out.println("Task10");
        System.out.println(max);
        //task11
        max = -5;
        for (double k = -5; k < 5; k +=0.01){
            result = Math.pow(k, 2.71) + k;
            if (result >max){
                max = result;
            }
        }
        System.out.println("Task11");
        System.out.println(max);
        //task12
        max = -10;
        for (double k = -10; k < 10; k +=0.01){
            result = Math.pow(k, 2.71) + Math.pow(k, 2);
            if (result >max){
                max = result;
            }
        }
        System.out.println("Task12");
        System.out.println(max);
    }
}
