package Lesson12.Task1;

import java.util.Scanner;

public class Main {
    public static void main(String [] args){

        Scanner in = new Scanner(System.in);

        for (int i = 0; i < 5; i++) {

            System.out.print("Day of week: ");
            Day day = Day.valueOf(in.next().toUpperCase());

            System.out.print("Month: ");
            Month month = Month.valueOf(in.next().toUpperCase());

            System.out.print("Meet: ");
            String meet = in.next();

            System.out.print("Meet description: ");
            String meetDescription = in.next();

            System.out.print("Hour: ");
            int hour = in.nextInt();

            System.out.print("Minutes: ");
            int minutes = in.nextInt();

            System.out.print("Day of month: ");
            int dayOfMonth = in.nextInt();

            DateOfMeet dateOfMeet1 = new DateOfMeet(day,month,meet,meetDescription,hour,minutes,dayOfMonth);
            dateOfMeet1.show();
        }

    }
}
enum Day{
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY
}
enum Month{
    JANUARY,
    FEBRUARY,
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY,
    AUGUST,
    SEPTEMBER,
    OCTOBER,
    NOVEMBER,
    DECEMBER
}
class DateOfMeet{

    private Day day;
    private Month month;
    private String meet;
    private String meetDescription;
    private int hour;
    private int minutes;
    private int num;

    public DateOfMeet(Day day, Month month, String meet, String meetDescription, int hour, int minutes, int num) {
        this.day = day;
        this.month = month;
        this.meet = meet;
        this.meetDescription = meetDescription;
        this.hour = hour;
        this.minutes = minutes;
        this.num = num;
    }

    void show(){
        System.out.println("Day of week: " + day);
        System.out.println("Month: " + month);
        System.out.println("Meet: " + meet);
        System.out.println("Meet description: " + meetDescription);
        System.out.println("Hour: " + hour);
        System.out.println("Minutes: " + minutes);
        System.out.println("Day of month: " + num);
    }
}


