public class Moon {
    public static void main(String[] args){
        double weightEarth;
        double humanMass = 60;
        double weightMoon;
        double accelerationEarth = 9.8;

        weightEarth = humanMass*accelerationEarth;
        weightMoon = (weightEarth*17)/100;
        System.out.println("Ваш вес на Луне будет равен " + weightMoon + " Н при 17% от силы тяжести на Земле");
    }
}
