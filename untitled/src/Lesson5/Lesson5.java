package Lesson5;

public class Lesson5 {

    public static void main(String[] args) {

        int[] array = {3, 4, 2, 1, 5, 7, 6, 8, 9, 12, 10, 11};
        int[][] twoDimArray = {
                {1, 3, 5},
                {7, 5, 6},
                {8, 12, 1}
        };
        arrayMin(array); //минимальное число
        arrayMax(array); //максимальное число
        arraySum(array);  //сумма чисел в массиве
        arrayMidSum(array); //Среднее арифметическое
        arrayMultiply(array); //Произведение элементов
        arrayOddSum(array); //Сумма элементов с нечетными номерами
        arrayEvenSum(array); //Сумма элементов с четными номерами
        arrayOddMultiply(array); //Произведение элементов с нечетными номерами
        arrayEvenMultiply(array); //Произведение элементов с четными номерами
        arrayZero(array); //Количество элементов равных нулю
        arrayCountOdd(array); //Количество нечетных элементов
        arrayCountEven(array); //Количество четных элементов
        arrayCompareMore(array);//Количество элементов больших за k
        arrayCompareLess(array);//Количество элементов меньших за k
        arrayNearZero(array);//Число, которое равное нулю или близкое к нему
        arrayNorm(array);//
        arrayMinTwoDim(twoDimArray);//Минимальное число в двумерном масиве
        arrayMaxTwoDim(twoDimArray);//Максимальное число в двумерном масиве равно
        arraySumTwoDim(twoDimArray);//Сумма чисел в двумерном масиве равна
        arraySumMidTwoDim(twoDimArray);//Среднее арифметическое двумерного массива
        arraySumMainDig(twoDimArray);//Сумма чисел в главной диагонали в двумерном массиве
        arraySumCollateralDig(twoDimArray);//Сумма чисел в побочной диагонали в двумерном масиве равна
        arrayNormTwoDim(twoDimArray);//
        arrayDeterminant(twoDimArray);//Определитель матрицы
        arrayTrans(twoDimArray);//Транспонирование матрицы

        int[][] twoDimArray4 = {
                {2, 5, 7},
                {6, 3, 4},
                {5, -2, -3}
        };
        arrayInverse(twoDimArray4);
    }


    static void arrayMin(int[] array) {
        int min = array[0];
        for (int i = 0; i < array.length; i++) {
            if (min > array[i]) {
                min = array[i];
            }
        }
        System.out.println("Минимальное число = " + min);
    }

    static void arrayMax(int[] array) {
        int max = array[0];
        for (int i = 0; i < array.length; i++) {
            if (max < array[i]) {
                max = array[i];
            }
        }
        System.out.println("Максимальное число = " + max);
    }

    static void arraySum(int[] array) {
        int total = 0;
        for (int i = 0; i < array.length; i++) {
            total += array[i];
        }
        System.out.println("Сумма элементов в одномерном массиве  = " + total);
    }

    static void arrayMidSum(int[] array) {
        int t = 0;
        for (int i = 0; i < array.length; i++) {
            t += array[i];
        }
        t = t / array.length;
        System.out.println("Среднее арифметическое = " + t);
    }

    static void arrayMultiply(int[] array) {
        int multiply = 1;
        for (int i = 0; i < array.length; i++) {
            multiply *= array[i];
        }
        System.out.println("Произведение элементов = " + multiply);
    }

    static void arrayOddSum(int[] array) {
        int total = 0;
        for (int i = 0; i < array.length; i++) {
            if (!(i % 2 == 0)) {
                total += array[i];
            }
        }
        System.out.println("Сумма элементов с нечетными номерами = " + total);
    }

    static void arrayEvenSum(int[] array) {
        int total = 0;
        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 0) {
                total += array[i];
            }
        }
        System.out.println("Сумма элементов с четными номерами = " + total);
    }

    static void arrayOddMultiply(int[] array) {
        int total = 1;
        for (int i = 0; i < array.length; i++) {
            if (!(i % 2 == 0)) {
                total *= array[i];
            }
        }
        System.out.println("Произведение элементов с нечетными номерами = " + total);
    }

    static void arrayEvenMultiply(int[] array) {
        int total = 1;
        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 0) {
                total *= array[i];
                //System.out.println(array[i]);
            }
        }
        System.out.println("Произведение элементов с четными номерами = " + total);
    }

    static void arrayZero(int[] array) {
        int totalZero = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                totalZero += 1;
            }
        }
        System.out.println("Количество элементов равных нулю = " + totalZero);
    }

    static void arrayCountOdd(int[] array) {
        int totalOdd = 0;
        for (int i = 0; i < array.length; i++) {
            if (!(i % 2 == 0)) {
                totalOdd += 1;
            }
        }
        System.out.println("Количество нечетных элементов = " + totalOdd);
    }

    static void arrayCountEven(int[] array) {
        int totalEven = 0;
        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 0) {
                totalEven += 1;
            }
        }
        System.out.println("Количество четных элементов = " + totalEven);
    }

    static void arrayCompareMore(int[] array) {
        int k = array[0];
        int total = 0;
        for (int i = 0; i < array.length; i++) {
            if (k < array[i]) {
                total += 1;
            }
        }
        System.out.println("Количество элементов больших за " + k + " = " + total);
    }

    static void arrayCompareLess(int[] array) {
        int k = array[0];
        int total = 0;
        for (int i = 0; i < array.length; i++) {
            if (k > array[i]) {
                total += 1;
            }
        }
        System.out.println("Количество элементов меньших за " + k + " = " + total);
    }

    static void arrayNearZero(int[] array) {
        int k = array[0];
        int o = 0;
        for (int i = 0; i < array.length; i++) {
            if (Math.abs(array[i]) < Math.abs(k)) {
                k = array[i];
                o = i;
            }

        }
        System.out.println("Число, которое равное нулю или близкое к нему,  находится под индексом " + o + "( значение = " + k + " ) ");
    }

    static void arrayNorm(int[] array) {
        double max = 0;
        double total = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[0] < array[i]) {
                max = array[i];
            }
        }
        for (int i = 0; i < array.length; i++) {
            total = array[i] / max;
            System.out.print(total + " ");
        }
        System.out.println();
    }

    static void arrayMinTwoDim(int[][] twoDimArray) {
        int min = 0;
        min = twoDimArray[0][0];
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[i].length; j++) {
                if (min > twoDimArray[i][j]) {
                    min = twoDimArray[i][j];
                }
            }
        }
        System.out.println("Минимальное число в двумерном масиве равно = " + min);
    }

    static void arrayMaxTwoDim(int[][] twoDimArray) {
        int max = 0;
        max = twoDimArray[0][0];
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[i].length; j++) {
                if (max < twoDimArray[i][j]) {
                    max = twoDimArray[i][j];
                }
            }
        }
        System.out.println("Максимальное число в двумерном масиве равно = " + max);
    }

    static void arraySumTwoDim(int[][] twoDimArray) {
        int total = 0;
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[i].length; j++) {
                total += twoDimArray[i][j];
            }
        }
        System.out.println("Сумма чисел в двумерном масиве равна = " + total);
    }

    static void arraySumMidTwoDim(int[][] twoDimArray) {
        double totalSum = 0;
        double count = 0;
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[i].length; j++) {
                count = twoDimArray.length * twoDimArray[i].length;
                totalSum += twoDimArray[i][j];
            }
        }
        totalSum = totalSum / count;
        System.out.println("Среднее арифметическое двумерного массива = " + totalSum);
    }

    static void arraySumMainDig(int[][] twoDimArray) {
        int total = 0;
        int start = twoDimArray[0][0];
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[i].length; j++) {
                if (i == j) {
                    start = twoDimArray[i][j];
                    total += start;
                }
            }
        }
        System.out.println("Сумма чисел в главной диагонали в двумерном массиве равна = " + total);
    }

    static void arraySumCollateralDig(int[][] twoDimArray) {
        int total = 0;
        for (int i = twoDimArray.length - 1; i >= 0; i--) {
            for (int j = 0; j < twoDimArray[i].length; j++) {
                if (i + j == twoDimArray.length - 1) {
                    total += twoDimArray[i][j];

                }
            }
        }
        System.out.println("Сумма чисел в побочной диагонали в двумерном массиве равна = " + total);
    }

    static void arrayNormTwoDim(int[][] twoDimArray) {
        double max = twoDimArray[0][0];
        double norm = 0;
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[i].length; j++) {
                if (twoDimArray[0][0] < twoDimArray[i][j]) {
                    max = twoDimArray[i][j];
                }
            }
        }
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[i].length; j++) {
                norm = twoDimArray[i][j] / max;
                System.out.print(norm + " | ");
            }
        }
        System.out.println();
    }

    static void arrayTrans(int[][] twoDimArray) {
        System.out.println("Матрица до транспонирования:");
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[i].length; j++) {
                System.out.print(twoDimArray[i][j] + " | ");
            }
            System.out.println();
        }

        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = i + 1; j < twoDimArray[i].length; j++) {
                int num = twoDimArray[i][j];
                twoDimArray[i][j] = twoDimArray[j][i];
                twoDimArray[j][i] = num;
            }

        }
        System.out.println("Матрица после транспонирования:");
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[i].length; j++) {
                System.out.print(twoDimArray[i][j] + " | ");
            }
            System.out.println();
        }
    }

    static void arrayDeterminant(int[][] twoDimArray) {
        int determinant1 = 0;
        int determinant2 = 0;
        int determinant3 = 0;
        int total = 0;

        determinant1 = +twoDimArray[0][0]*(twoDimArray[1][1]*twoDimArray[2][2] - twoDimArray[1][2]*twoDimArray[2][1]);
        determinant2 = -twoDimArray[0][1]*(twoDimArray[1][0]*twoDimArray[2][2] - twoDimArray[2][0]*twoDimArray[1][2]);
        determinant3 = +twoDimArray[0][2]*(twoDimArray[1][0]*twoDimArray[2][1] - twoDimArray[2][0]*twoDimArray[1][1]);

        total = determinant1 + determinant2 + determinant3;
        System.out.println("Определитель матрицы = " + total);
    }

    static void arrayInverse (int [][] array){

        System.out.println(" Найти обратную матрицу для квадратного двумерного массива:");
        int determinant1 = 0;
        int determinant2 = 0;
        int determinant3 = 0;
        int total = 0;
        int [][] array1 = new int[3][3];


        //Находим определитель матрицы
        determinant1 = +array[0][0]*(array[1][1]*array[2][2] - array[1][2]*array[2][1]);
        determinant2 = -array[0][1]*(array[1][0]*array[2][2] - array[2][0]*array[1][2]);
        determinant3 = +array[0][2]*(array[1][0]*array[2][1] - array[2][0]*array[1][1]);

        total = determinant1 + determinant2 + determinant3;

        System.out.println("Определитель матрицы: " + total);

        //Находим матрицу миноров
        array1[0][0] = array[1][1]*array[2][2] - array[2][1]*array[1][2];
        if (array[1][0]*array[2][2] - array[2][0]*array[1][2] > 0){
            array1[0][1] = -(array[1][0]*array[2][2] - array[2][0]*array[1][2]);
        }else {
            array1[0][1] = Math.abs(array[1][0] * array[2][2] - array[2][0] * array[1][2]);
        }
        array1[0][2]= array[1][0]*array[2][1] - array[2][0]*array[1][1];
        if (array[0][1]*array[2][2] - array[2][1]*array[0][2] > 0) {
            array1[1][0] = - (array[0][1] * array[2][2] - array[2][1] * array[0][2]);
        }else {
            array1[1][0] = Math.abs(array[0][1] * array[2][2] - array[2][1] * array[0][2]);
        }
        array1[1][1] = array[0][0]*array[2][2] - array[2][0]*array[0][2];
        if (array[0][0]*array[2][1] - array[2][0]*array[0][1] > 0) {
            array1[1][2] = - (array[0][0] * array[2][1] - array[2][0] * array[0][1]);
        }else{
            array1[1][2] = Math.abs(array[0][0] * array[2][1] - array[2][0] * array[0][1]);
        }
        array1[2][0]= array[0][1]*array[1][2] - array[1][1]*array[0][2];
        if (array[0][0]*array[1][2] - array[1][0]*array[0][2] > 0) {
            array1[2][1] = - (array[0][0] * array[1][2] - array[1][0] * array[0][2]);
        }else{
            array1[2][1] = Math.abs(array[0][0] * array[1][2] - array[1][0] * array[0][2]);
        }
        array1[2][2]= array[0][0]*array[1][1] - array[1][0]*array[0][1];

        System.out.println("Матрица миноров:");
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array1[i].length; j++) {
                System.out.print(array1[i][j] + " | ");
            }
            System.out.println();
        }

        //Находим транспонированную матрицу
        for (int i = 0; i < array1.length; i++) {
            for (int j = i + 1; j < array1[i].length; j++) {
                int temp = array1[i][j];
                array1[i][j] = array1[j][i];
                array1[j][i] = temp;
            }

        }
        System.out.println("Матрица миноров после транспонирования:");
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array1[i].length; j++) {
                System.out.print(array1[i][j] + " | ");
            }
            System.out.println();
        }

        System.out.println("Обратная матрица:");
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array1[i].length; j++) {
                array1[i][j] = (1/total)*array1[i][j];
                System.out.print(array1[i][j] + " | ");
            }
            System.out.println();
        }

        System.out.println("Проверка:");

        int [][] arrayOne = new int[3][3];

        arrayOne[0][0] = array[0][0]*array1[0][0] + array[0][1]*array1[1][0] + array[0][2]*array1[2][0];
        arrayOne[0][1] = array[0][0]*array1[0][1] + array[0][1]*array1[1][1] + array[0][2]*array1[2][1];
        arrayOne[0][2] = array[0][0]*array1[0][2] + array[0][1]*array1[1][2] + array[0][2]*array1[2][2];

        arrayOne[1][0] = array[1][0]*array1[0][0] + array[1][1]*array1[1][0] + array[1][2]*array1[2][0];
        arrayOne[1][1] = array[1][0]*array1[0][1] + array[1][1]*array1[1][1] + array[1][2]*array1[2][1];
        arrayOne[1][2] = array[1][0]*array1[0][2] + array[1][1]*array1[1][2] + array[1][2]*array1[2][2];

        arrayOne[2][0] = array[2][0]*array1[0][0] + array[2][1]*array1[1][0] + array[2][2]*array1[2][0];
        arrayOne[2][1] = array[2][0]*array1[0][1] + array[2][1]*array1[1][1] + array[2][2]*array1[2][1];
        arrayOne[2][2] = array[2][0]*array1[0][2] + array[2][1]*array1[1][2] + array[2][2]*array1[2][2];

        for (int i = 0; i < arrayOne.length; i++) {
            for (int j = 0; j < arrayOne[i].length; j++) {
                System.out.print(arrayOne[i][j] + " | ");
            }
            System.out.println();
        }

    }

}


