import java.util.Scanner;

public class Lesson3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        if (in.nextInt() == 10) {
            System.out.println("Верно!");
        } else {
            System.out.println("Неверно!");
        }


        if (in.nextInt() == 0) {
            System.out.println("Верно!");
        } else {
            System.out.println("Неверно!");
        }

        if (in.nextBoolean()) {
            System.out.println("Верно!");
        } else {
            System.out.println("Неверно!");
        }

        int num = in.nextInt();

        if (num > 0 & num < 16) {
            System.out.println("Первая четверть!");
        } else if (num > 15 & num < 31) {
            System.out.println("Вторая четверть!");
        } else if (num > 30 & num < 46) {
            System.out.println("Третья четверть!");
        } else if (num > 45 & num < 60) {
            System.out.println("Четвертая четверть!");
        }

        String result;
        int enter = in.nextInt();

        switch (enter) {
            case 1:
                result = "Зима";
                System.out.println(result);
                break;
            case 2:
                result = "Весна";
                System.out.println(result);
                break;
            case 3:
                result = "Лето";
                System.out.println(result);
                break;
            case 4:
                result = "Осень";
                System.out.println(result);
                break;
        }

        int month = in.nextInt();

        if (month > 0 & month < 3 | month == 12) {
            System.out.println("Зима");
        } else if (month > 3 & month < 6) {
            System.out.println("Весна");
        } else if (month > 6 & month < 9) {
            System.out.println("Лето");
        } else if (month > 9 & month < 12) {
            System.out.println("Весна");
        }


        int day = in.nextInt();

        if (day > 1 & day < 11){
            System.out.println("1 deacada");
        } else if (day > 10 & day < 21){
            System.out.println("2 deacada");
        } else if (day > 20 & day < 32){
            System.out.println("3 deacada");
        }
    }
}