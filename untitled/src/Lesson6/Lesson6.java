package Lesson6;

public class Lesson6 {
    public static void main(String[] args) {

        int[][] array = {
                {1, 3, 5},
                {7, 5, 6},
                {8, 12, 1}
        };
        arraySortMinTwoDim(array);//Сортировка двумерного массива по убыванию
        arraySortMaxTwoDim(array);//Сортировка двумерного массива по возростанию

        int[][] arraySortMinVer = {
                {1, 3, 5},
                {7, 5, 6},
                {8, 12, 1}
        };
        arraySortMinVer(arraySortMinVer);//Сортировка столбцов массива по убиванию
        int[][] arraySortMaxVer = {
                {1, 3, 5},
                {7, 5, 6},
                {8, 12, 1}
        };
        arraySortMaxVer(arraySortMaxVer);//Сортировка столбцов массива по возрастанию

        int[][] arraySortGorMax = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        arraySortGorMax(arraySortGorMax);//Сортировка строк в двумерном массиве по возрастанию суммы четных элементов в них

        int[][] arraySortGorMin = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        arraySortGorMin(arraySortGorMin);//Сортировка строк в двумерном массиве по убыванию суммы четных элементов в них


        int[][] arrayCountGorPrime = {
                {4, 4, 2},
                {4, 2, 4},
                {4, 4, 4}
        };
        arrayCountGorPrime(arrayCountGorPrime);//Количество строк, содержащих простые числа, в двумерном массиве
        int[][] arrayCountVerPrime = {
                {4, 4, 4},
                {1, 2, 2},
                {4, 4, 4}
        };
        arrayCountVerPrime(arrayCountVerPrime);//Количество столбцов, содержащих простые числа, в двумерном массиве

        int[][] array1 = {{1, 3, 5}, {7, 5, 6}, {8, 12, 1}};
        int[][] array2 = {{1, 3, 5}, {7, 5, 6}, {8, 12, 1}};
        arrayMultiplyArray(array1,array2);
    }

    static void arraySortMinTwoDim(int[][] twoDimArray) {
        int[] arrayTest = new int[9];
        int num = 1;
        int p = 0;
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[i].length; j++) {
                arrayTest[p] = twoDimArray[i][j];
                num = arrayTest[p];
                p += 1;
                //System.out.print(num + " | ");
            }
        }
        //System.out.println();
        for (int i = 0; i < arrayTest.length; i++) {
            for (int j = i + 1; j < arrayTest.length; j++) {
                if (arrayTest[i] < arrayTest[j]) {
                    int temp = arrayTest[i];
                    arrayTest[i] = arrayTest[j];
                    arrayTest[j] = temp;
                }
            }
            //System.out.print(arrayTest[i] + " | ");
        }
        System.out.println("Сортировка двумерного массива по убыванию: ");
        p = 0;
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[i].length; j++) {
                twoDimArray[i][j] = arrayTest[p];
                num = twoDimArray[i][j];
                p += 1;
                System.out.print(twoDimArray[i][j] + " | ");
            }
            System.out.println();
        }
    }
    static void arraySortMaxTwoDim(int[][] twoDimArray) {
        int[] arrayTest = new int[9];
        int num = 1;
        int p = 0;
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[i].length; j++) {
                arrayTest[p] = twoDimArray[i][j];
                num = arrayTest[p];
                p += 1;
                //System.out.print(num + " | ");
            }
        }
        //System.out.println();
        for (int i = 0; i < arrayTest.length; i++) {
            //System.out.print(arrayTest[i]);
            for (int j = i + 1; j < arrayTest.length; j++) {
                if (arrayTest[i] > arrayTest[j]) {
                    int temp = arrayTest[i];
                    arrayTest[i] = arrayTest[j];
                    arrayTest[j] = temp;
                }
                //System.out.print(arrayTest[i] + " | ");
            }
            //System.out.print(arrayTest[i] + " | ");
        }
        System.out.println("Сортировка двумерного массива по возростанию: ");
        p = 0;
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[i].length; j++) {
                twoDimArray[i][j] = arrayTest[p];
                num = twoDimArray[i][j];
                p += 1;
                System.out.print(twoDimArray[i][j] + " | ");
            }
            System.out.println();
        }
    }


    static void arraySortMinVer(int[][] twoDimArray) {
        int sum1 = 0;
        int sum2 = 0;
        int sum3 = 0;
        int[] array1 = new int[3];
        int[] array2 = new int[3];
        int[][] arraySort = new int[3][3];

        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[i].length; j++) {
                if (j == 0) {
                    sum1 += twoDimArray[i][j];
                    array1[j] = sum1;
                    array2[j] = j;
                } else if (j == 1) {
                    sum2 += twoDimArray[i][j];
                    array1[j] = sum2;
                    array2[j] = j;
                } else if (j == 2) {
                    sum3 += twoDimArray[i][j];
                    array1[j] = sum3;
                    array2[j] = j;
                }
            }
        }
        for (int i = 0; i < array1.length; i++) {
            for (int j = i + 1; j < array1.length; j++) {
                if (array1[i] < array1[j]) {
                    int temp = array1[i];
                    array1[i] = array1[j];
                    array1[j] = temp;

                    int temp1 = array2[i];
                    array2[i] = array2[j];
                    array2[j] = temp1;
                }
            }
            //System.out.println(array1[i]);
            //System.out.println(array2[i]);
        }
        System.out.println("Сортировка столбцов массива по убиванию:");
        for (int i = 0; i < arraySort.length; i++) {
            for (int j = 0; j < arraySort.length; j++) {
                arraySort[i][j] = twoDimArray[i][array2[j]];
                System.out.print(arraySort[i][j] + " | ");
            }
            System.out.println();
        }
    }
    static void arraySortMaxVer(int[][] twoDimArray) {
        int sum1 = 0;
        int sum2 = 0;
        int sum3 = 0;
        int[] array1 = new int[3];
        int[] array2 = new int[3];
        int[][] arraySort = new int[3][3];

        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[i].length; j++) {
                if (j == 0) {
                    sum1 += twoDimArray[i][j];
                    array1[j] = sum1;
                    array2[j] = j;
                } else if (j == 1) {
                    sum2 += twoDimArray[i][j];
                    array1[j] = sum2;
                    array2[j] = j;
                } else if (j == 2) {
                    sum3 += twoDimArray[i][j];
                    array1[j] = sum3;
                    array2[j] = j;
                }
            }
        }
        for (int i = 0; i < array1.length; i++) {
            for (int j = i + 1; j < array1.length; j++) {
                if (array1[i] > array1[j]) {
                    int temp = array1[i];
                    array1[i] = array1[j];
                    array1[j] = temp;

                    int temp1 = array2[i];
                    array2[i] = array2[j];
                    array2[j] = temp1;
                }
            }
            //System.out.println(array1[i]);
            //System.out.println(array2[i]);
        }
        System.out.println("Сортировка столбцов массива по возрастанию:");
        for (int i = 0; i < arraySort.length; i++) {
            for (int j = 0; j < arraySort[i].length; j++) {
                arraySort[i][j] = twoDimArray[i][array2[j]];
                System.out.print(arraySort[i][j] + " | ");
            }
            System.out.println();
        }
    }


    static void arraySortGorMax(int[][] array) {
        System.out.println();
        int sum = 0;
        int[] array1 = new int[3];
        int[] array2 = new int[3];
        int[][] arraySort = new int[3][3];
        for (int i = 0; i < array.length; i++) {
            sum = 0;
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] % 2 == 0) {
                    sum += array[i][j];
                }
            }
            array1[i] = sum;
            array2[i] = i;
        }
        for (int i = 0; i < array1.length; i++) {
            for (int j = i + 1; j < array1.length; j++) {
                if (array1[i] > array1[j]) {
                    int temp = array1[i];
                    array1[i] = array1[j];
                    array1[j] = temp;

                    int tempI = array2[i];
                    array2[i] = array2[j];
                    array2[j] = tempI;
                }
            }
        }
        System.out.println("Сортировка строк в двумерном массиве по возрастанию суммы четных элементов в них:");
        for (int i = 0; i < arraySort.length; i++) {
            for (int j = 0; j < arraySort[i].length; j++) {
                arraySort[i][j] = array[array2[i]][j];
                System.out.print(arraySort[i][j] + " | ");
            }
            System.out.println();
        }
    }
    static void arraySortGorMin(int[][] array) {
        System.out.println();
        int sum = 0;
        int[] array1 = new int[3];
        int[] array2 = new int[3];
        int[][] arraySort = new int[3][3];
        for (int i = 0; i < array.length; i++) {
            sum = 0;
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] % 2 == 0) {
                    sum += array[i][j];
                }
            }
            array1[i] = sum;
            array2[i] = i;
        }
        for (int i = 0; i < array1.length; i++) {
            for (int j = i + 1; j < array1.length; j++) {
                if (array1[i] < array1[j]) {
                    int temp = array1[i];
                    array1[i] = array1[j];
                    array1[j] = temp;

                    int tempI = array2[i];
                    array2[i] = array2[j];
                    array2[j] = tempI;
                }
            }
        }
        System.out.println("Сортировка строк в двумерном массиве по убыванию суммы четных элементов в них:");
        for (int i = 0; i < arraySort.length; i++) {
            for (int j = 0; j < arraySort[i].length; j++) {
                arraySort[i][j] = array[array2[i]][j];
                System.out.print(arraySort[i][j] + " | ");
            }
            System.out.println();
        }
    }


    static void arrayCountGorPrime (int [][] array) {
        System.out.println("Количество строк, содержащих простые числа, в двумерном массиве: ");
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] != 1) {
                    boolean flag = false;
                    for (int k = 2; k < array[i][j]; k++) {
                            if (array[i][j] % k == 0) {
                                flag = true;
                                break;
                            }
                    }
                    if (!flag) {
                        count++;
                    }
                }
            }

        }
        System.out.println(count);
    }
    static void arrayCountVerPrime (int [][] array){
        System.out.println("Количество столбцов, содержащих простые числа, в двумерном массиве:");
        int count = 0;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (array[j][i] != 1){
                    boolean flag = false;
                    for (int k = 2; k < array[j][i]; k++) {
                        if (array[j][i] % 2 == 0){
                            flag = true;
                            break;
                        }
                    }
                    if (!flag) {
                        count++;
                    }
                }
            }
        }
        System.out.println(count);
    }

    static void arrayMultiplyArray (int [][] array1, int [][] array2){

        int[][] arrayMultiply = new int[3][3];

        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array1[i].length; j++) {
                for (int k = 0; k < array2.length; k++) {
                        arrayMultiply[i][j] += array1[i][k]*array2[k][j];
                }
            }
        }
        System.out.println("Результат умножения двух квадратных матриц:");
        for (int i = 0; i < arrayMultiply.length; i++) {
            for (int j = 0; j < arrayMultiply[0].length; j++) {
                System.out.print(arrayMultiply[i][j] + " | ");
            }
            System.out.println();
        }
    }
}
