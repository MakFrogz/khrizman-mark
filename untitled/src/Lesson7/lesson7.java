package Lesson7;

public class lesson7 {
    public static void main(String[] args) {
        String[] abc = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
        int[] array = {1, 13, 6, 8, 9, 11, 17, 27, 30};
        int[] arrayEqual = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
        int[][] arrayTwoDim = {
                {6, 3, 8},
                {6, 4, 12},
                {4, 8, 9}
        };
        int[][][] arrayFillIn = new int[3][3][3];
        int [][][] array3x3x3 = {{{1,2,3},{4,5,7},{6,-9,1}},{{7,2,3},{4,-12,7},{6,9,1}},{{1,24,3},{4,5,7},{-6,1,1}}};

        //func
        arrayMax(array); //Максимальное число в массиве
        arrayMin(array);//Минимальное число в массиве
        arraySumAfterOne(array);//найти сумму элементов, следующих после элемента, равного единице, вывести эту сумму
        arrayTwoDimMinMainDiag(arrayTwoDim);//Минимальное число в двумерном массиве главной диагонали
        arrayFillIn(arrayFillIn);//заполнение массива
        arrayElementsEqual(arrayEqual);//Проверка, что все элементы в массиве одинаковы
        arraySumPlus(array);//Сумма положительных элементов в массиве
        arrayCheckAscending(array);//Элементы массива расположены по возрастанию
        arrayFindThreeMax(array);//Три самых больших элемента массива

        int[] array1 = {1, 13, 6, 8, 9, 11, 17, 27, 30};
        arrayInverse(array1);//Перевернутый массив
        int[] array2 = {1, 13, 6, 8, 9, 11, 17, 27, 30};
        arrayShift(array2);//Сдвинутый массив вправо
        int[] array3 = {1, 13, 6, 8, 9, 11, 17, 27, 30};
        arrayNotEqual(array3);//Все элементы массива разные
        int[] array4 = {1, 17, 17, 13, 17, 11, 17, 17, 17};
        arrayCountNotEqual(array4);//Количество различных элементов в массиве
        int[] array5 = {1, 13, 6, 8, 4, 14, 17, 27, 30};
        arrayCountZero(array5);//Количество элементов больших за ноль
        int[] array6 = {1, 13, 6, 8, 4, 14, 17, 27, 30};
        arrayCountMoreNum(array6);//Количество элементов больших за k
        double[] array7 = {1, 13, 6, -8, 4, -14, 17, 27, 30};
        arrayNorm(array7);

        arraySumTwoDim(arrayTwoDim);//Сумма всех элементов двумерного массива
        arrayMidSumTwoDim(arrayTwoDim);//Сумма главной диагонали в двумерном массиве
        arraySumMainCollDiag(arrayTwoDim);//Сумма побочной диагонали в двумерном массиве
        double[][] arrayTwoDim1 = {
                {6, 3, 8},
                {6, 4, 12},
                {4, 8, 9}
        };
        arrayMaxTwoDim(arrayTwoDim1);//
        double[][] arrayTwoDim2 = {
                {6, 24, 8},
                {20, 4, 12},
                {4, 8, 9}
        };
        arrayDivide2(arrayTwoDim2);//Поделить на два все элементы двумерного массива, большие десяти по модулю
        arraySum3x3x3(array3x3x3);//Сумма элементов в трехмерном массиве
        arrayChangeSymbol(array3x3x3);//поменять знак отрицательных элементов
        double [][][] array3x3x3Sum = {{{1,-2,3},{4,5,7},{6,-9,1}},{{7,2,3},{4,-12,7},{6,-9,1}},{{1,-24,3},{4,5,7},{-6,1,1}}};
        arraySumMidNegative(array3x3x3Sum);//Среднее арифметическое  для всех неотрицательных элементов трехмерного массива
        int [][][] arrayTest = {{{1,2,3},{4,5,7},{6,-9,1}},{{7,2,3},{4,-12,7},{6,9,1}},{{1,24,3},{4,5,7},{-6,1,1}}};
        arraySort3x3x3(arrayTest);//сортировка массива по возрастанию

        int [][][] arraySortXYmax = {{{1,12,3},{4,5,7},{6,-9,1}},{{7,2,3},{4,-12,7},{6,9,1}},{{1,24,3},{4,5,7},{-6,1,1}}};
        arraySortXYmax(arraySortXYmax);//Сортировка по возрастанию xy-плоскости трехмерного массива по сумме элементов в них

        int [][][] arrayPrint = {{{1,12,3},{4,5,7},{6,-9,1}},{{7,2,3},{4,-12,7},{6,9,1}},{{1,24,3},{4,5,7},{-6,1,1}}};
        arrayPrint(arrayPrint);// вывод элементов трехмерного массива в строку через пробел.
        int [][][] arraySortMaxToTwoDim = {{{1,12,3},{4,5,7},{6,-9,1}},{{7,2,3},{4,-12,7},{6,9,1}},{{1,24,3},{4,5,7},{-6,1,4}}};
        arraySortMaxToTwoDim(arraySortMaxToTwoDim);
        int [][][] arrayPrintWithZ = {{{1,12,3},{4,5,7},{6,-9,1}},{{7,2,3},{4,-12,7},{6,9,1}},{{1,24,3},{4,5,7},{-6,1,4}}};
        arrayPrintWithZ(arrayPrintWithZ);
        int [][][] arrayMidSum = {{{1,12,3},{4,5,7},{6,-9,1}},{{7,2,3},{4,-12,7},{6,9,1}},{{1,24,3},{4,5,7},{-6,1,4}}};
        arrayMidSum(arrayMidSum);
        int [][][] arraySortMidSum = {{{1,12,3},{4,5,7},{6,-9,1}},{{7,2,3},{4,-12,7},{6,9,1}},{{1,24,3},{4,5,7},{-6,1,4}}};
        arraySortMidSum(arraySortMidSum);
    }

    static void arrayMax(int[] array) {
        int max = array[0];
        for (int i = 0; i < array.length; i++) {
            if (max < array[i]) {
                max = array[i];
            }
        }
        System.out.println("Максимальное число в массиве = " + max);
    }

    static void arrayMin(int[] array) {
        int min = array[0];
        for (int i = 0; i < array.length; i++) {
            if (min > array[i]) {
                min = array[i];
            }
        }
        System.out.println("Минимальное число в массиве = " + min);
    }

    static void arraySumAfterOne(int[] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 1) {
                sum += array[i + 1];
            }
        }
        if (sum != 0) {
            System.out.println("Сумма = " + sum);
        } else {
            System.out.println("Сумма = " + sum);
        }
    }

    static void arrayTwoDimMinMainDiag(int[][] arrayTwoDim) {

        int min = arrayTwoDim[0][0];
        for (int i = 0; i < arrayTwoDim.length; i++) {
            for (int j = 0; j < arrayTwoDim[i].length; j++) {
                if (i == j) {
                    if (min > arrayTwoDim[i][j]) {
                        min = arrayTwoDim[i][j];
                    }
                }
            }
        }
        System.out.println("Минимальное число в двумерном массиве главной диагонали = " + min);
    }

    static void arrayFillIn(int[][][] arrayFillIn) {
        int i = 0;
        int j = 0;
        int k = 0;
        while (i < arrayFillIn.length) {
            j = 0;
            while (j < arrayFillIn[i].length) {
                k = 0;
                while (k < arrayFillIn[j].length) {
                    arrayFillIn[i][j][k] = 1;
                    System.out.print(arrayFillIn[i][j][k] + " | ");
                    k++;
                }
                j++;
            }
            i++;
        }
        System.out.println();
    }

    static void arrayElementsEqual(int[] arrayEqual) {
        int start = arrayEqual[0];
        int count = 0;
        for (int i = 0; i < arrayEqual.length; i++) {
            if (start == arrayEqual[i]) {
                count++;
            }
        }
        if (count / arrayEqual.length == 1) {
            System.out.println("True");
        } else {
            System.out.println("False");
        }
    }

    static void arraySumPlus(int[] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > 0) {
                sum += array[i];
            }
        }
        System.out.println("Сумма положительных элементов в массиве = " + sum);
    }

    static void arrayCheckAscending(int[] array) {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (i != array.length - 1) {
                if (array[i] < array[i + 1]) {
                    count++;
                }
            }
        }
        System.out.print("Элементы массива расположены по возрастанию: ");
        if (count == array.length - 1) {
            System.out.print("True");
        } else {
            System.out.print("False");
        }
    }

    static void arrayFindThreeMax(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] < array[j]) {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }

            }
        }
        System.out.println();
        System.out.println("Три самых больших элемента массива : ");
        for (int i = 0; i < 3; i++) {
            System.out.print(array[i] + " | ");
        }
    }

    static void arrayInverse(int[] array) {
        System.out.println();
        System.out.println("Перевернутый массив: ");
        for (int i = 0; i < array.length / 2; i++) {
            int temp = array[(array.length - 1) - i];
            array[(array.length - 1) - i] = array[i];
            array[i] = temp;
        }
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " | ");
        }
    }

    static void arrayShift(int[] array) {
        System.out.println();
        System.out.println("Сдвинутый массив вправо: ");
        int [] arrayShift = new int[array.length];
            for (int i = 0; i < array.length; i++) {
                for (int j = i + 1; j < arrayShift.length; j++) {
                    if (j == 8) {
                        arrayShift[0] = array[j];
                    }
                    arrayShift[j] = array[i];
                }
                //System.out.println(temp);
                System.out.print(arrayShift[i] + " | ");
        }
    }

    static void arrayNotEqual (int[] array){
        System.out.println();
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (i != j){
                    if (array[i] != array[j]){
                        count++;
                    }
                }
            }
        }
        System.out.println("Все элементы массива разные:");
        if (count/(array.length*(array.length-1)) == 1){
            System.out.print("True");
        }else{
            System.out.print("False");
        }
    }

    static void arrayCountNotEqual (int [] array){
        System.out.println();
        int count = 0;
        int total = 0;
        for (int i = 0; i < array.length; i++) {
            count = 0;
            for (int j = 0; j < array.length; j++) {
                if (i != j){
                    if (array[i] != array[j]){
                        count++;
                    }
                }
            }
            if (count/(array.length-1) == 1){
                total++;
            }
        }
        System.out.println("Количество различных элементов в массиве = " + total);
    }

    static void arrayCountZero (int [] array){
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > 0){
                count++;
            }
        }
        System.out.println("Количество элементов больших за ноль = " + count);
    }

    static void arrayCountMoreNum (int [] array){
        int k = 10;
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > k){
                count++;
            }
        }
        System.out.println("Количество элементов больших за " + k + " = " + count);
    }

    static void arrayNorm (double [] array){
        double sum = 0;
        double norm = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] >= 0){
                sum +=array[i];
            }
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] >= 0){
                array[i] = array[i] / sum;
            }
            System.out.print(array[i] + " | ");
        }
    }

    static void arraySumTwoDim (int [][] array){
        System.out.println();
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                sum += array[i][j];
            }
        }
        System.out.println("Сумма всех элементов двумерного массива равна " + sum);
    }

    static void arrayMidSumTwoDim (int [][] array){
        System.out.println();
        double sum = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                sum += array[i][j];
            }
        }
        sum = sum/9;
        System.out.println("Среднее арифметическое двумерного массива = " + sum);
    }

    static void arraySumMainCollDiag (int [][] array){
        int sumMain = 0;
        int sumColl = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (i == j) {
                    sumMain += array[i][j];
                }
                if (i + j == 2) {
                    sumColl += array[i][j];
                }
            }
        }
        System.out.println("Сумма главной диагонали в двумерном массиве = " + sumMain + "\nСумма побочной диагонали в двумерном массиве = " + sumColl);
    }

    static void arrayMaxTwoDim (double [][] array){
        double max = array[0][0];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (max < array[i][j]){
                    double temp = max;
                    max = array[i][j];
                    array[i][j] = temp;
                }
            }
        }
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = array[i][j]/max;
                System.out.print(array[i][j] + " | ");
            }
        }
    }

    static void arrayDivide2 (double [][] array){
        System.out.println();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] > 10){
                    array[i][j] = array[i][j]/2;
                }
                System.out.print(array[i][j] + " | ");
            }
        }
    }

    static void arraySum3x3x3 (int [][][] array){
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                for (int k = 0; k < array[j].length; k++) {
                    sum += array[i][j][k];
                }
            }

        }
        System.out.println();
        System.out.println("Сумма элементов в трехмерном массиве = " + sum);
    }

    static void arrayChangeSymbol (int [][][] array){
        System.out.println("Поменять знак у всех отрицательных элементов трехмерного массива: ");
        for (int i = 0; i < array.length; i++) {
            System.out.println();
            for (int j = 0; j < array[i].length; j++) {
                System.out.println();
                for (int k = 0; k < array[j].length; k++) {
                    if (array[i][j][k] < 0){
                        array[i][j][k] = Math.abs(array[i][j][k]);
                        //System.out.print(array[i][j][k] + " | ");
                    }
                    System.out.print(array[i][j][k] + " | ");
                }
            }
        }
    }

    static void arraySumMidNegative (double [][][] array){
        System.out.println();
        double sum = 0;
        double count = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                for (int k = 0; k < array[j].length; k++) {
                    if (array[i][j][k] > 0){
                        sum += array[i][j][k];
                        count++;
                    }
                }
            }
        }
        sum = sum/count;
        System.out.println("Среднее арифметическое  для всех неотрицательных элементов трехмерного массива = " + sum);
    }

    static void arraySort3x3x3 (int [][][] array){
        System.out.println("Отсортировать по возрастанию элементы трехмерного массива: ");
        int [] arraySort3x3x3 = new int [27];
        int p = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                for (int k = 0; k < array[j].length; k++) {
                    arraySort3x3x3[p] = array[i][j][k];
                    p++;
                }
            }
        }
        for (int i = 0; i < arraySort3x3x3.length; i++) {
            for (int j = i + 1; j < arraySort3x3x3.length; j++) {
                if (arraySort3x3x3[i] > arraySort3x3x3[j]){
                    int temp = arraySort3x3x3[i];
                    arraySort3x3x3[i] = arraySort3x3x3[j];
                    arraySort3x3x3[j] = temp;
                }
            }
        }
        p = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                for (int k = 0; k < array[j].length; k++) {
                    array[i][j][k] = arraySort3x3x3[p];
                    p++;
                    System.out.print(array[i][j][k] + " | ");
                }
            }
        }
    }

    static void arraySortXYmax (int [][][] array){
        System.out.println();
        System.out.println("Отсортированный по возрастанию xy-плоскости трехмерный массива по сумме элементов в них: ");
        int sum1 = 0;
        int sum2 = 0;
        int sum3 = 0;
        int [] arraySum = new int[3];
        int [] arrayIndex = new int[3];
        int [][][] arraySort = new int[3][3][3];

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                for (int k = 0; k < array[j].length; k++) {
                    if (i == 0){
                        sum1 += array[i][j][k];
                        arraySum[i] = sum1;
                        arrayIndex[i] = i;
                    }
                    if (i == 1){
                        sum2 += array[i][j][k];
                        arraySum[i] = sum2;
                        arrayIndex[i] = i;
                    }
                    if (i == 2){
                        sum3 += array[i][j][k];
                        arraySum[i] = sum3;
                        arrayIndex[i] = i;
                    }
                }
            }
        }

        for (int i = 0; i < arraySum.length; i++) {
            for (int j = i + 1; j < arraySum.length; j++) {
                if (arraySum[i] > arraySum[j]){
                    int tempSum = arraySum[i];
                    arraySum[i] = arraySum[j];
                    arraySum[j] = tempSum;

                    int tempIndex = arrayIndex[i];
                    arrayIndex[i] = arrayIndex[j];
                    arrayIndex[j] = tempIndex;
                }
            }
        }

//        for (int i = 0; i < arraySum.length; i++) {
//            System.out.print(arraySum[i] + " | ");
//            System.out.print(arrayIndex[i] + " | ");
//        }
        for (int i = 0; i < arraySort.length; i++) {
            for (int j = 0; j < arraySort[i].length; j++) {
                for (int k = 0; k < arraySort[j].length; k++) {
                    arraySort[i][j][k] = array[arrayIndex[i]][j][k];
                    System.out.print(arraySort[i][j][k] + " | ");
                }
                System.out.println();
            }
            System.out.println();
        }
    }

    static void arrayPrint (int [][][] array){
        System.out.println("Вывести элементы трехмерного массива в строку через пробел:");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                for (int k = 0; k < array[j].length; k++) {
                    System.out.print(array[i][j][k] + " ");
                }
            }
        }
    }

    static void arraySortMaxToTwoDim (int [][][] array){
        System.out.println();
        int[][] arrayTemp = new int[3][3];
        //int max = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                int max = 0;
                for (int k = 0; k < array[j].length; k++) {
                    if (max < array[i][j][k]){
                        max = array[i][j][k];
                    }
                }
                arrayTemp[i][j] = max;
            }
        }
        System.out.println("Выписать в двумерный массив элементы xy-плоскости трехмерного массива, содержащей максимальный элемент в данном трехмерном массиве: ");
        for (int i = 0; i < arrayTemp.length; i++) {
            for (int j = 0; j < arrayTemp[i].length; j++) {
                System.out.print(arrayTemp[i][j] + " | ");
            }
            System.out.println();
        }
    }

    static void arrayPrintWithZ (int [][][] array){
        System.out.println("Последовательно вывести элементы xy-плоскостей трехмерного массива, перед выводом каждой из них указывать z-координату для выводимой плоскости (z = 0, z = 1 и т.д.): ");

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                for (int k = 0; k < array[j].length; k++) {
                    System.out.print(array[i][j][k] + " | ");
                }
                System.out.println();
                //System.out.println(i);
            }
            System.out.println("Z = " + i);

        }
    }

    static void arrayMidSum (int [][][] array){

        System.out.println("Вычислить средние арифметические для элементов каждой из xy-плоскостей трехмерного массива и вывести их через запятую: ");
        double sum = 0;

        for (int i = 0; i < array.length; i++) {
            //sum = 0;
            for (int j = 0; j < array[i].length; j++) {
                sum = 0;
                for (int k = 0; k < array[j].length; k++) {
                    sum += array[i][j][k];
                }
                sum = sum/array.length;
                System.out.print(sum + ", ");
            }
        }

        //System.out.println(sum + " | ");
    }

    static void arraySortMidSum (int [][][] array){
        System.out.println();
        System.out.println("Отсортировать по возрастанию xy-плоскости трехмерного массива по возрастанию среднего арифметического элементов каждой из данных xy-плоскостей: ");

        int count = 0;
        double sum = 0;
        double[] arraySum = new double[3];
        int[] arrayIndex = new int[3];
        int [][][] arraySort = new int[3][3][3];

        for (int i = 0; i < array.length; i++) {
            sum = 0;
            count = 0;
            for (int j = 0; j < array[i].length; j++) {
                for (int k = 0; k < array[j].length; k++) {
                    sum += array[i][j][k];
                    count++;
                }
            }
            sum = sum/count;
            arraySum[i] = sum;
            arrayIndex[i] = i;

        }

        for (int i = 0; i < arraySum.length; i++) {
            for (int j = i + 1; j < arraySort.length; j++) {
                if (arraySum[i] > arraySum[j]){
                    double tempSum = arraySum[i];
                    arraySum[i] = arraySum[j];
                    arraySum[j] = tempSum;

                    int tempIndex = arrayIndex[i];
                    arrayIndex[i] = arrayIndex[j];
                    arrayIndex[j] = tempIndex;
                }
            }
        }

        for (int i = 0; i < arraySort.length; i++) {
            for (int j = 0; j < arraySort[i].length; j++) {
                for (int k = 0; k < arraySort[j].length; k++) {
                    arraySort[i][j][k] = array[arrayIndex[i]][j][k];
                    System.out.print(arraySort[i][j][k] + " | ");
                }
                System.out.println();
            }
            //System.out.println();
            System.out.println("Среднее арифметическое ху-плоскости = " + arraySum[i]);
        }
    }
}