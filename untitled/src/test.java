import java.util.Scanner;

public class test {
    public static void main(String[] args) {
        int[][] arr = {{1, 5, 7}, {6, 8, 2}, {8, 4, 5}};


        System.out.println("Сортировка двумерного массива по возрастанию: ");
        for (int k = 0; k < arr.length; k++) {
            for (int p = 0; p < arr[k].length; p++) {
                int z = p + 1;
                for (int m = k; m < arr.length; m++) {
                    for (int y = z; y < arr[m].length; y++) {
                        if (arr[m][y] > arr[k][p]) {
                            int temp = arr[k][p];
                            arr[k][p] = arr[m][y];
                            arr[m][y] = temp;
                        }
                    }
                    z = 0;
                }
                System.out.print(arr[k][p] + "\t");
            }
            System.out.println();
        }
    }
}

