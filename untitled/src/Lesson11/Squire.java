package Lesson11;

public class Squire implements Test{
    @Override
    public void sayAnything() {
        System.out.println("Take this sword!");
    }

    @Override
    public void doAnything() {
        System.out.println("Fearing...");
    }

    @Override
    public void die() {
        System.out.println("No.....I am so young!");
    }

    @Override
    public void sleep() {
        System.out.println("Ok, today!");
    }
}
