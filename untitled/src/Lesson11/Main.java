package Lesson11;

public class Main {
    public static void main(String [] args){
        SwordMan swordman = new SwordMan();
        swordman.sayAnything();
        swordman.doAnything();
        swordman.die();
        swordman.sleep();

        System.out.println();

        Knight knight = new Knight();
        knight.sayAnything();
        knight.doAnything();
        knight.die();
        knight.sleep();

        System.out.println();

        Squire squire = new Squire();
        squire.sayAnything();
        squire.doAnything();
        squire.die();
        squire.sleep();
    }
}
