package Lesson11;

public class SwordMan implements Test {

    @Override
    public void sayAnything() {
        System.out.println("I use for my sword for king!");
    }

    @Override
    public void doAnything() {
        System.out.println("Slash!");
    }

    @Override
    public void die() {
        System.out.println("Not today, my old friend!");
    }

    @Override
    public void sleep() {
        System.out.println("Ok!");
    }
}
