package Lesson11.Task3;

public class OrcPeon extends Orc{

    int hp;
    int damage;
    int armor;
    String typeAttack;

    OrcPeon(int hp, int damage, int armor, String typeAttack){
        super(hp, damage, armor, typeAttack);
    }

    @Override
    void Attack() {
        System.out.println("Damage = " + super.damage);
        System.out.println("Type attack = " + super.typeAttack);
    }
}
