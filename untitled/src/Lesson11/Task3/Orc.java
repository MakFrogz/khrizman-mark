package Lesson11.Task3;

public abstract class Orc {

    int hp;
    int damage;
    int armor;
    String typeAttack;

    Orc(int hp, int damage, int armor, String typeAttack){
        this.hp = hp;
        this.damage = damage;
        this.armor = armor;
        this.typeAttack = typeAttack;
    }

    abstract void Attack();
}
