package Lesson11.Task3;

public class Main {
    public static void main(String [] args){

        OrcPeon orcPeon = new OrcPeon(50,1,3,"crushing");
        orcPeon.Attack();

        OrcGrunt orcGrunt = new OrcGrunt(100,10,5,"hacking");
        orcGrunt.Attack();

        OrcShaman orcShaman = new OrcShaman(70,6,3,"magical");
        orcShaman.Attack();

    }
}
