package Lesson11;

public class Knight implements Test {
    @Override
    public void sayAnything() {
        System.out.println("I love my horse!");
    }

    @Override
    public void doAnything() {
        System.out.println("Charge!");
    }

    @Override
    public void die() {
        System.out.println("It was interesting life...");
    }

    @Override
    public void sleep() {
        System.out.println("Not today!");
    }
}
