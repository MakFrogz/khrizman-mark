package Lesson11;

public interface Test {

    void sayAnything();
    void doAnything();
    void die();
    void sleep();

}
