package Lesson11.Game;

import java.util.Scanner;

public class Game {

    Human human = new Human();
    Computer computer = new Computer();
    Scanner input = new Scanner(System.in);

    Game(String humanName, String computerName) {
        human.setName(humanName);
        computer.setName(computerName);
    }

    void play() {
        System.out.println(human.getName());
        System.out.println(computer.getName());

        System.out.println("Enter your choice: ");
        System.out.println("1 - Rock");
        System.out.println("2 - Paper");
        System.out.println("3 - Scissors");

            int playerInput = input.nextInt();
            int computerInput = computer.getMove();
           // System.out.println(computerInput);

            switch (playerInput) {
                case 1:
                    if (playerInput == computerInput) {
                        System.out.println("Draw!");
                    } else if (computerInput == 2) {
                        computer.addPoint();
                    } else if (computerInput == 3) {
                        human.addPoint();
                    }
                    break;
                case 2:
                    if (playerInput == computerInput) {
                        System.out.println("Draw!");
                    } else if (computerInput == 3) {
                        computer.addPoint();
                    } else if (computerInput == 1) {
                        human.addPoint();
                    }
                    break;
                case 3:
                    if (playerInput == computerInput) {
                        System.out.println("Draw!");
                    } else if (computerInput == 1) {
                        computer.addPoint();
                    } else if (computerInput == 2) {
                        human.addPoint();
                    }
                    break;
            }

            System.out.println(human.getName());
            human.getPoints();
            System.out.println(computer.getName());
            computer.getPoints();

            System.out.println("Enter 1, if you want to play again! Enter 0,if you want to go out!");

            int playerChoice = input.nextInt();

            boolean flag = true;
            while (flag) {
                if (playerChoice == 1) {
                    flag = true;
                    play();
                    break ;
                } else {
                   flag = false;
                   break;
                }
            }


    }
}
