package Lesson11.Game;

public class Computer implements Player {

    private String name;
    private int num;
    private int point;

    @Override
    public String  getName() {
        return name;
    }

    @Override
    public int getMove() {
        return this.num = (int)(Math.random()*3 + 1);
    }

    @Override
    public int addPoint() {
        return point++;
    }

    @Override
    public void getPoints() {
        System.out.println("Points: " + point);
    }

    public void setName(String name){
        this.name = name;
    }


}
