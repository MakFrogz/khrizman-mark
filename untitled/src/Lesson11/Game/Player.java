package Lesson11.Game;

public interface Player {


    String getName();
    int getMove();
    int addPoint();
    void getPoints();
}
