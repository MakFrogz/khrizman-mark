package Lesson11.Task2;

public abstract  class Rodent {
    String sex;
    String colorSkin;
    int weight;
    int speed;

    Rodent(String sex, String colorSkin, int weight, int speed){
        this.sex = sex;
        this.colorSkin = colorSkin;
        this.weight = weight;
        this.speed = speed;
    }

    abstract void Run();
    abstract void Jump();
    abstract void Eat();
}
