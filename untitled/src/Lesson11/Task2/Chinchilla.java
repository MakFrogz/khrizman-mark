package Lesson11.Task2;

public class Chinchilla extends Rodent {

    Chinchilla(String sex, String colorSkin, int weight, int speed){
        super(sex, colorSkin, weight, speed);
    }
    @Override
    void Run() {
        System.out.println("Right");
    }

    @Override
    void Jump() {

    }

    @Override
    void Eat() {

    }
    void info(){
        System.out.println("Speed: " + speed);
        System.out.println("Class name: " + getClass().getSimpleName());
    }
}
