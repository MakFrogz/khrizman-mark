package Lesson11.Task2;

public class Main {
    public static void main(String[] args){
        Hamster hamster = new Hamster("Male", "Red", 4,300);
        hamster.info();
        hamster.Run();

        System.out.println();

        Chinchilla chinchilla = new Chinchilla("Female","Blue",3, 200);
        chinchilla.info();
        chinchilla.Run();

        System.out.println();

        Rat rat = new Rat("Male","Green", 6, 500);
        rat.info();
        rat.Run();
    }
}
