package Lesson11.Task1;

public class HomelessDog extends Dog {

    String area;

    HomelessDog(String name, String sex, int weight, String area){
        super(name, sex, weight);
        this.area = area;
    }

    @Override
    void Voise() {
        System.out.println("Voise:" + "Ufff-ufff-ufff");
    }

    @Override
    void Action() {
        System.out.println("Action: " + "Attack");
    }
    void info(){
        System.out.println("Name: " + name);
        System.out.println("Sex: " + sex);
        System.out.println("Weight: " + weight);
        System.out.println("Area: " + area);
    }
}
