package Lesson11.Task1;


public class DomesticDog extends Dog {

    String address;

    DomesticDog(String name, String sex, int weight, String address){
        super(name,sex,weight);
        this.address = address;
    }

    @Override
    void Voise(){
        System.out.println("Voise:" + "Gav-gav-gav");
    }

    @Override
    void Action(){
        System.out.println("Action: " + "Sit down");
    }

    void info(){
        System.out.println("Name: " + name);
        System.out.println("Sex: " + sex);
        System.out.println("Weight: " + weight);
        System.out.println("Address: " + address);
    }
}
