package Lesson11.Task1;

import java.util.SortedMap;

public class Main {
    public static void main(String[] args){
        DomesticDog domesticDog = new DomesticDog("Ben", "Male", 20,"Israel");
        domesticDog.info();
        domesticDog.Voise();
        domesticDog.Action();

        System.out.println();
        ServiceDog serviceDog = new ServiceDog("Masha", "Female", 13,"Masa");
        serviceDog.info();
        serviceDog.Voise();
        serviceDog.Action();

        System.out.println();
        HomelessDog homelessDog = new HomelessDog("Ignat", "Male", 19, "Home");
        homelessDog.info();
        homelessDog.Voise();
        homelessDog.Action();
    }
}
