package Lesson11.Task1;

public abstract class Dog {

    String name;
    String sex;
    int weight;

    Dog(String name, String sex, int weight){
        this.name = name;
        this.sex = sex;
        this.weight = weight;
    }

    abstract void Voise();
    abstract void Action();
}
