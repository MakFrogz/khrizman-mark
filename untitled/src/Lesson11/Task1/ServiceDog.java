package Lesson11.Task1;

public class ServiceDog extends Dog {

    String organization;

    ServiceDog(String name, String sex, int weight, String organization){
        super(name, sex, weight);
        this.organization = organization;
    }

    @Override
    void Voise() {
        System.out.println("Voise:" + "Tyav-tyav-tyav!");
    }

    @Override
    void Action() {
        System.out.println("Action: " + "Bring brunch");
    }
    void info(){
        System.out.println("Name: " + name);
        System.out.println("Sex: " + sex);
        System.out.println("Weight: " + weight);
        System.out.println("Organization: " + organization);
    }
}
