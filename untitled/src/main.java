public class main {
    public static void main(String[] args){
        int[] array = {1, 5, 6, 8, 9, 11, 17, 17, 30};

        int[] arrayInverse = new int[9];
        for (int i = 0; i < array.length/2; i++) {
            int temp = array[(array.length - 1) - i];
            array[(array.length - 1) - i] = array[i];
            array[i] = temp;
        }
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " | ");
        }
    }
}
