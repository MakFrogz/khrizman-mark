public class Exam {
    public static void main (String [] args){

        int [][] array = {
                {1,4,7},
                {6,3,9},
                {8,3,9}
        };
        int min = array[0][0];
        for (int i = 0; i < array.length; i++) {
                    if (min > array[i][i]){
                        min = array[i][i];
                    }
        }
        System.out.println(min);
    }
}
