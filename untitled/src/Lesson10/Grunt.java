package Lesson10;

public class Grunt extends Peon{

    int damage;
    int armor;

    Grunt(int damage, int armor, int hp, int food){
        super(hp, food);
        this.damage = damage;
        this.armor = armor;
    }

    void strike(){
        System.out.println("Lok tar o`gar!");
    }

    void sayAnything(){
        System.out.println("For the Horde!");
    }
}
