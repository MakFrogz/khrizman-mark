package Lesson10.Task2;

public class Buy extends Product {

    private int count;

    Buy(){

    }

    Buy(String name, int weight, int price, int count){
        super(name, weight, price);
        this.count = count;
    }
    int priceAll(){
        return count*getPrice();
    }

    int weightAll(){
        return count*getWeight();
    }

    int getCount(){
        return count;
    }

    void setCount (int count){
        this.count = count;
    }

}
