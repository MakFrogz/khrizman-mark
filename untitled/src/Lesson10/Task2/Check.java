package Lesson10.Task2;

public class Check extends Buy {

    Check(String name, int weight, int price, int count){
        super(name, weight, price, count);
    }

    void info(){
        System.out.println("Количество товара: " + getCount());
        System.out.println("Общая цена: " + priceAll());
        System.out.println("Общий вес: " + weightAll());
    }
}
