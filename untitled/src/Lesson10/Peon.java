package Lesson10;

public class Peon {
    private int hp;
    int food;
    int wood;

    Peon(int hp, int food){
        this.hp = hp;
        this.food = food;
        wood = 0;
    }

    void getWood(){
        while (wood < 11) {
            System.out.println("I got wood!");
            wood++;
        }
    }

    void sayAnything(){
        System.out.println("Zak-zak!");
    }

    public void setHp(int hp) {
        this.hp = hp;
    }
}
