package Lesson8;

public class Task3 {
    public static void main(String[] args){

        Variable val = new Variable();//task3
        val.info();
        val.changeVariable(7,8);
        val.info();
        int sum = val.sum(6,14);
        System.out.println(sum);
        System.out.println(val.compare());
    }
}
class Variable {

    int x;
    int y;

    void info(){
        System.out.println(x);
        System.out.println(y);
    }

    void changeVariable(int a, int b){
        this.x = a;
        this.y = b;
    }

    int sum (int x, int y){
        return x + y;
    }

    int compare() {
        if (this.x > this.y) {
            return x;
        } else {
            return y;
        }
    }
}
