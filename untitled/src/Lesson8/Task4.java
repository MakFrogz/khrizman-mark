package Lesson8;

public class Task4 {
    public static void main (String [] args){

        String[] names = {"Mark", "Arseniy", "Misha", "Kostya", "Lena", "Olya", "Oleg", "Masha", "Ignat", "Sasha"};
        String[] surnames = {"Khrizman", "Popov", "Banan", "Kapusta", "Stol", "Gayka", "Okno", "Babkov", "Petrov", "Pupkin"};
        Student [] students = new Student[10];
        int [][] marks = new int[students.length][5];
        for (int i = 0; i < marks.length; i++) {
            for (int j = 0; j < marks[i].length; j++) {
                marks[i][j] = (int)(Math.random()*3+3);
            }
        }

        for (int i = 0; i < students.length; i++) {
                students[i] = new Student(names[i], surnames[i], (int)(Math.random()*3 + 3), marks[i]);
        }

        for (int i = 0; i < students.length; i++) {
            students[i].info();
        }

        System.out.println();

        for (int i = 0; i < students.length; i++) {
            students[i].showTheBestStudents();
        }

    }

}

class Student{

    String name;
    String surname;
    int groupNumber;
    int [] marks;


    void info(){
        System.out.println();
        System.out.println("Имя: " + name);
        System.out.println("Фамилия: " + surname);
        System.out.println("Номер группы: " + groupNumber);
        System.out.print("Оценки: ");
        for (int i = 0; i < marks.length; i++) {
            System.out.print(marks[i] + " | ");
        }
        System.out.println();
    }

    void showTheBestStudents(){
        int count = 0;
        for (int i = 0; i < marks.length; i++) {
            if (marks[i] >= 4){
                count++;
            }
        }
        if (count/marks.length == 1){
            System.out.println("Лучший ученик этого класса: ");
            System.out.println("Имя: " + name);
            System.out.println("Фамилия: " + surname);
            System.out.println("Номер группы: " + groupNumber);
            System.out.print("Оценки: ");
            for (int j = 0; j < marks.length; j++) {
                System.out.print(marks[j] + " | ");
            }
            System.out.println();
        }
    }

    Student(String names, String surnames, int groupNumber, int[] marks){
        this.name = names;
        this.surname = surnames;
        this.groupNumber = groupNumber;
        this.marks = marks;
    }

    void show(){
        for (int i = 0; i < marks.length; i++) {
            System.out.println(name + " " + marks[i] + " | ");
        }
    }
}
