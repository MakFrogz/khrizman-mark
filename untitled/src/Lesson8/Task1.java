package Lesson8;

public class Task1 {
    public static void main(String [] args){
        Human mark = new Human("Mark", "Khrizman", 23, true);//task1
        mark.info();
    }
}
class Human{

    String name;
    String lastname;
    int age;
    boolean sex;

    void info(){
        System.out.println("Имя: " + name);
        System.out.println("Фамилия: " + lastname);
        System.out.println("Возраст: " + age);
        System.out.println("Пол мужской? " + sex);
    }
    Human (String name, String lastname, int age, boolean sex){
        this.name = name;
        this.lastname = lastname;
        this.age = age;
        this.sex = sex;
    }

}
