package Lesson8;

public class Task2 {
    public static void main (String [] args){
    Monster orfen = new Monster("Orfen", 5000, 1000, 500);//task2
    orfen.info();
}
}
class Monster {

    String name;
    int hp;
    int damage;
    int armor;

    void info() {
        System.out.println("Имя монстра: " + name);
        System.out.println("Количество очков жизни: " + hp);
        System.out.println("Урон монстра: " + damage);
        System.out.println("Броня: " + armor);
    }

    Monster(String name, int hp, int damage, int armor) {
        this.name = name;
        this.hp = hp;
        this.damage = damage;
        this.armor = armor;
    }
}