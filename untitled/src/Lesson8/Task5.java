package Lesson8;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        System.out.println("Привет! Это моя домашняя библиотека!");
        System.out.println("Введите одну из перечисленных команд: Add - добавить книгу, Remove - удалить книгу, List - список книг, Exit - выйти из библиотеки.");

        Scanner input = new Scanner(System.in);
        Library user = new Library();
        boolean flag = true;

        while (flag) {

            String in = input.next();

            if (in.equals("Add")) {

                System.out.print("Название книги: ");
                String bookName = input.next();

                System.out.print("Автор книги: ");
                String autor = input.next();

                System.out.print("Дата издания книги: ");
                String date = input.next();

                System.out.print("Количество страниц: ");
                String countPages = input.next();

                user.addToLibrary(bookName, autor, date, countPages);
            }

            if (in.equals("Remove")) {
                System.out.print("Название книги: ");
                String bookName = input.next();
                user.removeFromLibrary(bookName);
            }

            if (in.equals("Sort")){
                System.out.println("Введите одну из перечисленных команд сортировки: по названию книги - byBookName, по имени автора - byAutor, по дате издания - byDate, по количеству страниц - byCountPages");
                System.out.print("Сделать сортировку:");
                String sort = input.next();
                user.sortLibrary(sort);
            }

            if (in.equals("List")) {
                System.out.println("Список книг в библиотеке: ");
                user.info();
            }

            if (in.equals("Exit")) {
                flag = false;
            }
        }
    }
}

class Library{

    String bookName;
    String autor;
    String date;
    String countPages;
    Library[] array = new Library[0];

    Library(){

    }

    Library(String bookName, String autor, String date, String countPages){
        this.bookName = bookName;
        this.autor = autor;
        this.date = date;
        this.countPages = countPages;
    }

    void info(){
        for (Library i : array) {
            System.out.printf("Название книги:%s Автор:%s Дата издания книги:%s Количество страниц: %s\n",i.bookName, i.autor, i.date, i.countPages);
        }
    }

    void  addToLibrary(String bookName, String autor, String date, String countPages){
        Library[] arrayNew = new Library[array.length + 1];
        System.arraycopy(array, 0, arrayNew,0,array.length);
        arrayNew[arrayNew.length - 1] = new Library(bookName, autor, date, countPages);
        array = arrayNew;
    }

    void removeFromLibrary(String bookName){
        Library[] arrayNew = new Library[array.length - 1];
        for (int i = 0; i < array.length; i++) {
            if (array[i].bookName.equals(bookName)){
                System.arraycopy(array, 0, arrayNew, 0,array.length - (array.length - i));
                System.arraycopy(array, i + 1,arrayNew,i,array.length - (i + 1));
            }
        }
        array = arrayNew;
    }

    void sortLibrary(String sort){
        if (sort.equals("byBookName")){
            for (int i = 0; i < array.length; i++) {
                for (int j = i + 1; j < array.length; j++) {
                    if (array[i].bookName.compareTo(array[j].bookName) > 0){
                        Library temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                    }
                }
            }
        }

        if (sort.equals("byAutor")){
            for (int i = 0; i < array.length; i++) {
                for (int j = i + 1; j < array.length; j++) {
                    if (array[i].autor.compareTo(array[j].autor) > 0){
                        Library temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                    }
                }
            }
        }

        if (sort.equals("byDate")){
            for (int i = 0; i < array.length; i++) {
                for (int j = i + 1; j < array.length; j++) {
                    if (array[i].date.compareTo(array[j].date) > 0){
                        Library temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                    }
                }
            }
        }

        if (sort.equals("byCountPages")){
            for (int i = 0; i < array.length; i++) {
                for (int j = i + 1; j < array.length; j++) {
                    if (array[i].countPages.compareTo(array[j].countPages) > 0){
                        Library temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                    }
                }
            }
        }
    }
}


